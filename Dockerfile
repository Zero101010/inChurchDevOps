FROM python:3.8-alpine
WORKDIR /app

COPY SpeedObjectAPI/ . 

RUN pip install -r requirements.txt

ENTRYPOINT [ "python" ,"main.py" ]
