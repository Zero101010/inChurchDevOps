terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
}

provider "aws" {
  profile = "default"
  region = "us-west-1"
}

resource "aws_instance" "zeus" {
  ami           = "ami-031b673f443c2172c"
  instance_type = "t2.micro"
  tags = {
    Name = "zeus"
  }
}