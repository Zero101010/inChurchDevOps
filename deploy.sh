sshpass ssh -i "zeus.pem" ubuntu@ec2-54-193-57-48.us-west-1.compute.amazonaws.com <<-'ENDSSH'
   docker stop $(docker ps -q)
   docker rmi $(docker images -q) -f 
   docker container rm $(docker ps -aq)
   docker run -p 8080:8080 -d zero101010/velocityapi:latest 
ENDSSH
